///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example (count how long my son's been alive):
//
// Reference time: Thu May  4 02:12:00 2000
//
// Years: 20  Days: 279  Hours: 16  Minutes: 55  Seconds: 57 
// Years: 20  Days: 279  Hours: 16  Minutes: 55  Seconds: 58 
// Years: 20  Days: 279  Hours: 16  Minutes: 55  Seconds: 59 
// Years: 20  Days: 279  Hours: 16  Minutes: 56  Seconds: 0 
// Years: 20  Days: 279  Hours: 16  Minutes: 56  Seconds: 1 
// Years: 20  Days: 279  Hours: 16  Minutes: 56  Seconds: 2 
// Years: 20  Days: 279  Hours: 16  Minutes: 56  Seconds: 3 
//
// -- OR (count down towards a time in the future)
//
// Reference time: Sat May  4 02:12:00 2024
// 
// Years: 3  Days: 91  Hours: 6  Minutes: 58  Seconds: 3
// Years: 3  Days: 91  Hours: 6  Minutes: 58  Seconds: 2
// Years: 3  Days: 91  Hours: 6  Minutes: 58  Seconds: 1
// Years: 3  Days: 91  Hours: 6  Minutes: 58  Seconds: 0
// Years: 3  Days: 91  Hours: 6  Minutes: 57  Seconds: 59
// Years: 3  Days: 91  Hours: 6  Minutes: 57  Seconds: 58
// Years: 3  Days: 91  Hours: 6  Minutes: 57  Seconds: 57
//
// @author Mark Nelson <marknels@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

const int REFERENCE_YEAR   = 2000;
const int REFERENCE_MONTH  = 05;    // Jan = 01, Feb = 02, ... Dec = 12
const int REFERENCE_DAY    = 04;    // Day of the month
const int REFERENCE_HOUR   = 02;    // On a 24-hour clock
const int REFERENCE_MINUTE = 12;

const int secondsInMinute  = 60;
const int secondsInHour    = secondsInMinute * 60;
const int secondsInDay     = secondsInHour * 24;
const int secondsInYear    = secondsInDay * 365;


/// Start with the input parameter differenceInSeconds
///
/// Figure out how many whole years are in the seconds (put in differenceInYears)
///
/// Then, subtract that many years from the differenceInSeconds
/// 
/// Repeat for Days, Hours and Minutes 
///
/// labs :: long absolute value (from stdlib.h)
void printDifference( double differenceInSeconds ) {
   // printf ("%lf\n", differenceInSeconds);

   /// How many whole years are in differenceInSeconds?
   long differenceInYears    = (long) differenceInSeconds / secondsInYear;
        differenceInSeconds -=        differenceInYears   * secondsInYear;  // Remove whole number of years

   /// How many whole days are left in differenceInSeconds?
   long differenceInDays     = (long) differenceInSeconds / secondsInDay;
        differenceInSeconds -=        differenceInDays    * secondsInDay;    // Remove whole number of days

   /// How many whole hours are left in differenceInSeconds?
   long differenceInHours    = (long) differenceInSeconds / secondsInHour;
        differenceInSeconds -=        differenceInHours   * secondsInHour;  // Remove whole number of hours

   /// How many whole minutes are left in differenceInSeconds?
   long differenceInMinutes  = (long) differenceInSeconds / secondsInMinute;
        differenceInSeconds -=        differenceInMinutes * secondsInMinute;

   printf ("Years: %ld  Days: %ld  Hours: %ld  Minutes: %ld  Seconds: %ld \n"
      ,labs( differenceInYears )
      ,labs( differenceInDays )
      ,labs( differenceInHours )
      ,labs( differenceInMinutes )
      ,labs( differenceInSeconds ));
}


int main() {

   time_t referenceTime = -1;

   struct tm reference;

   memset( &reference, 0, sizeof( reference ));

   reference.tm_year = REFERENCE_YEAR - 1900;  ///< per the spec:  "Year - 1900"
   reference.tm_mon  = REFERENCE_MONTH - 1;  ///< Jan=0 ... May=4
   reference.tm_mday = REFERENCE_DAY;
   reference.tm_hour = REFERENCE_HOUR;
   reference.tm_min  = REFERENCE_MINUTE;

   // Convert the tm structure into epoch time
   referenceTime = mktime( &reference );

   if( referenceTime == -1 ) {
      printf ("Can't make a reference time.  Exiting.\n" );
      exit(EXIT_FAILURE);
   }

   printf("Reference time: %s\n", asctime(&reference));

   time_t currentTime;
   double differenceInSeconds;

   while (1) {
      currentTime = time(NULL);

      differenceInSeconds = difftime( referenceTime, currentTime );

      printDifference( differenceInSeconds );
      
      // Wait for 1 second
      sleep(1);
   }

   return EXIT_SUCCESS;
}
